var app = angular.module('app', ["ngRoute"]);
var keyboardBinding;

app.config(function($routeProvider) {
    // Home
    $routeProvider.when("/pendu", {
        templateUrl: "partials/pendu.html",
        controller: "PenduController",
        resolve: {}
    });
    $routeProvider.when("/accueil", {
        templateUrl: "partials/accueil.html",
        controller: "AccueilController",
        resolve: {}
    });
    $routeProvider.otherwise({
        redirectTo: "/pendu",
    });
});

app.controller('DefaultController', function($scope) {
    console.log("DefaultController init");

    $scope.status = "default";

    $scope.account = {
        pseudo: ""
    };

    $scope.selectedWord = {
        length: 0,
        word: "",
        letters: []
    };

    $scope.try = {
        max: 8,
        current: 0,
        founded: 0
    };

    $scope.wordList = [];

    $scope.validKey = function(key) {
        for (var i = 0; i < config.keynoard.length; i++) {
            if (config.keynoard[i].key == key) {
                return true;
            }
        }
        return false;
    }

    for (var i = 0; i < config.wordList.length; i++) {
        var letters = [];

        for (var l = 0; l < config.wordList[i].length; l++) {
            var current = config.wordList[i][l];
            var toCheck, discovered;;

            if ($scope.validKey(current)) {
                toCheck = true;
                discovered = false;
            } else {
                toCheck = false;
                discovered = true;
            }

            letters.push({
                toCheck: toCheck,
                discovered: discovered,
                content: current
            });

        }

        $scope.wordList.push({
            length: config.wordList[i].length,
            word: config.wordList[i],
            letters: letters
        });
    }

});

app.controller('PenduController', function($scope, $location) {
    console.log("PenduController init");
    if ($scope.account.pseudo == "") {
        console.log("you're not identified !");
        $location.path("/accueil");
    }

    $scope.newWord = function() {
        // get random index
        var ri = Math.floor(Math.random() * (config.wordList.length));
        $scope.selectedWord = angular.copy($scope.wordList[ri]);
        $scope.try.current = 0;
        $scope.status = "game";
    }

    $scope.newWord();

    $scope.tryThisMLetter = function(letter) {
        console.log("tryThisMLetter", letter);
        var founded = false;

        $scope.try.founded = 0;

        for (var i = 0; i < $scope.selectedWord.letters.length; i++) {
            var current = $scope.selectedWord.letters[i];
            if (current.content == letter && !current.discovered) {
                current.discovered = true;
                founded = true;
            }
            if (current.discovered || !current.toCheck) {
                $scope.try.founded++;
            }
        }

        if (!founded) {
            $scope.try.current++;
        }

        if ($scope.try.current >= $scope.try.max) {
            console.log("La sentence est irrevocable");
            $scope.status = "perdu";
        }

        if ($scope.try.founded == $scope.selectedWord.length) {
            $scope.status = "gagne";

        }

        $scope.$apply();
    };

    window.onkeypress = function(event) {
        if ($scope.validKey(event.key) && $scope.status == "game") {
            $scope.tryThisMLetter(event.key);
        }

    };

});

app.controller('AccueilController', function($scope, $location) {
    console.log("AccueilController init");
    $scope.status = "pseudo";
    $scope.logmein = function() {
        console.log("logmein", $scope.formPseudo);
        if ($scope.formPseudo != undefined && $scope.formPseudo.length >= 3) {
            console.log("C'est parti !!!");
            $scope.account.pseudo = $scope.formPseudo;
            $location.path("/pendu");
        } else {
            console.log("non renseigné ou trop court");
        }
    }
});

app.controller('PerduController', function($scope, $location) {
    $scope.retry = function() {
        $location.path("/pendu");
    }
});
